import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import javax.swing.UIManager;


/*
Citation:
With help from https://stackoverflow.com/questions/14635952/java-layout-proportions-creating-a-scalable-square-panel
 */
public class ChessGUI {

    public static void main(String[] args) {
        new ChessGUI();
    }

    public ChessGUI() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception ex) {
                }

                JFrame frame = new JFrame("Test");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.add(new ChessBoardPane());
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    public class ChessBoardPane extends JPanel {

        public ChessBoardPane() {
            int index = 0;
            setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            for (int row = 0; row < 8; row++) {
                for (int col = 0; col < 8; col++) {
                    if(row ==6  || row == 1 ){
                        String piece = (row == 6) ? "\u265F" : "\u2659";
                        Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                        gbc.gridx = col;
                        gbc.gridy = row;
                        add(new Cell(color, piece), gbc);
                        //index++;
                    }
                    if( row == 0){
                        if(col == 0 || col == 7){
                            String piece = "\u2656";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 1 || col == 6){
                            String piece = "\u2658";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 2 || col == 5){
                            String piece = "\u2657";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 3){
                            String piece = "\u2655";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else{
                            String piece = "\u2654";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        }
                    }
                    if( row == 7){
                        if(col == 0 || col == 7){
                            String piece = "\u265C";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 1 || col == 6){
                            String piece = "\u265E";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 2 || col == 5){
                            String piece = "\u265D";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else if (col == 3){
                            String piece = "\u265B";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        } else{
                            String piece = "\u265A";
                            Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                            gbc.gridx = col;
                            gbc.gridy = row;
                            add(new Cell(color, piece), gbc);
                            //index++;

                        }
                    }else{
                        Color color = index % 2 == 0 ? Color.DARK_GRAY : Color.WHITE;
                        gbc.gridx = col;
                        gbc.gridy = row;
                        add(new Cell(color), gbc);
                        //index++;
                    }
                    index++;
                }
                index++;
            }
        }

    }

    public class Cell extends JButton {

        public Cell(Color background){
            setContentAreaFilled(false);
            setBorderPainted(false);
            setBackground(background);
            setOpaque(true);

        }

        public Cell(Color background, String text) {

            setContentAreaFilled(false);
            setBorderPainted(false);
            setBackground(background);
            setOpaque(true);
            setMargin(new Insets(0,0,0,0));
            setText(text);
            setFont(new Font("Arial", Font.PLAIN, 40));

        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(100, 100);
        }

    }

}
