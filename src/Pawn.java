import java.util.Scanner;


public class Pawn extends ChessPiece {
    private int x;
    private int y;
    private PieceColor color;
    private int dx;
    private boolean firstmove;

    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @param color of piece
     */
    public Pawn(int x, int y, PieceColor color){
        super(x,y,color);
        this.x = x;
        this.y = y;

        if(getColor() == PieceColor.WHITE){
            this.dx = 1;
        }else{
            this.dx = -1;
        }
        this.firstmove = true;
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX where we are moving piece to
     * @param moveToY where we are moving piece to
     * @return whether move was made
     */
    @Override
    public boolean makeMove(ChessBoard chessBoard, int moveToX, int moveToY) {
        if(this.isValidMove(chessBoard, moveToX, moveToY)){
            chessBoard.getChessBoard()[this.x][this.y] = null;
            this.x = moveToX;
            this.y = moveToY;
            chessBoard.getChessBoard()[moveToX][moveToY] = this;
            this.replacePawn((chessBoard));
            this.firstmove = false;
            return true;
        }
        return false;
    }
    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX where we want piece to move x
     * @param moveToY where we want piece to move y
     * @return whether legal move
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        if(moveToX >= chessBoard.getRow() || moveToX < 0 || moveToY >= chessBoard.getCol() || moveToY < 0){
            return false;
        }

        if(firstmove){
            //move forward first move
            if((moveToX == this.x + this.dx *2 || moveToX == this.x + this.dx) && moveToY == this.y &&  chessBoard.getChessBoard()[moveToX][moveToY] == null){
                return true;

            }else if( Math.abs(moveToY - this.y) == 1 && moveToX == this.x + this.dx && chessBoard.getChessBoard()[moveToX][moveToY] != null && chessBoard.getChessBoard()[moveToX][moveToY].getColor() != this.getColor()){
                return true;
            }else{
                return false;
            }
        }else{
            if(moveToX == this.x + this.dx && moveToY == this.y && chessBoard.getChessBoard()[moveToX][moveToY] == null){
                return true;
            }else if( Math.abs(moveToY - this.y) == 1 && moveToX == this.x + this.dx && chessBoard.getChessBoard()[moveToX][moveToY] != null && chessBoard.getChessBoard()[moveToX][moveToY].getColor() != this.getColor()){
                return true;
            }else{
                return false;
            }
        }

    }

    /**
     *
     * @param chessBoard chessboard we are playing on
     *       changes pawn we are playing with either Queen (1) or Knight (2) from user input
     */
    public void replacePawn(ChessBoard chessBoard){
        if((this.getColor() == PieceColor.WHITE && this.x == chessBoard.getRow() -1 ) || (this.getColor() == PieceColor.BLACK && this.x == 0)){
            Scanner sc = new Scanner(System.in);
            int replacePawnWith = sc.nextInt();
            if(replacePawnWith == 1){
                chessBoard.getChessBoard()[this.x][this.y] = new Queen(this.x, this.y, this.getColor());
            }
            else if(replacePawnWith == 2){
                chessBoard.getChessBoard()[this.x][this.y] = new Knight(this.x, this.y, this.getColor());
            }
        }
    }

    /**
     * character representation on board
     * @return
     */
    @Override
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'p';
        }
        return 'P';
    }


}
