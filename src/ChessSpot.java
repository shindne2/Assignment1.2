public class ChessSpot {
    private boolean occupied;
    private int x;
    private int y;
    private ChessPiece occupyingChessPiece;

    public ChessSpot ( int x, int y, ChessPiece chessPieceOccupying){
        this.occupied = (chessPieceOccupying != null) ;
        this.x = x;
        this.y = y;
        this.occupyingChessPiece = chessPieceOccupying;
    }

    public ChessSpot (int x, int y){
        this.occupied = false;
        this.x = x;
        this.y = y;
        this.occupyingChessPiece = null;
    }

    public boolean isOccupied(){
        return this.occupied;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public ChessPiece getOccupyingChessPiece(){
        return this.occupyingChessPiece;
    }

    public void changeOccupancyToEmpty(){
        if(this.occupied){
            this.occupied = false;
            this.occupyingChessPiece = null;
        }
    }

    public void changeOccupancy (ChessPiece chessPiece){
        assert(!this.occupied );
        this.occupied = true;
        this.occupyingChessPiece = chessPiece;
    }

}
