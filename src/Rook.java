public class Rook extends ChessPiece {
    private int x;
    private int y;
    private PieceColor color;

    /**
     *
     * @param x starting x coordinate
     * @param y starting y coordinate
     * @param color of piece
     */
    public Rook(int x, int y, PieceColor color){
        super(x,y,color);
        this.x = x;
        this.y = y;
        this.color = color;
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX where we want piece to move to x
     * @param moveToY where we want piece to move to y
     * @return
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        int rows = chessBoard.getRow();
        int cols = chessBoard.getCol();

        int checkX = super.x;
        int checkY = super.y;

        if((checkX - moveToX == 0 && checkY - moveToY == 0) || (checkX - moveToX != 0 && checkY-moveToY != 0) || moveToX >= rows || moveToX < 0 || moveToY > cols || moveToY < 0 || (moveToX == checkX && moveToY == checkY)){
            return false;
        }


        return checkColsRows(chessBoard, checkX, checkY, moveToX, moveToY);

    }

    /**
     *
     * @param chessBoard we are playing on
     * @param startX where the piece starts x
     * @param startY where the piece starts y
     * @param endX where we want piece to move to x
     * @param endY where we want piece to move to y
     * @return
     */
    public boolean checkColsRows(ChessBoard chessBoard, int startX, int startY, int endX, int endY){
        int dx = (endX == startX) ? 0 :  (endX - startX)/ Math.abs(endX - startX);
        int dy = (endY == startY) ? 0 : (endY - startY) / Math.abs(endY - startY);

        int x = startX;
        int y = startY;

        while((endX -dx != x && endY-dy == y) || (endX - dx == x && endY - dy != y) ){
            x += dx;
            y += dy;
            if(chessBoard.getChessBoard()[x][y] != null) {
                return false;
            }
        }
        if(chessBoard.getChessBoard()[endX][endY] != null && chessBoard.getChessBoard()[endX][endY].getColor() == this.getColor() ){
            return false;
        }
        return true;
    }

    /**
     *
     * @return character representation on board
     */
    @Override
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'r';
        }
        return 'R';
    }

}
