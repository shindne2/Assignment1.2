import java.lang.*;


public class ChessBoard {
    private int row;
    private int col;
    private ChessPiece[][] chessBoard;
    private int whiteKingX;
    private int whiteKingY;
    private int blackKingX;
    private int blackKingY;


    public ChessBoard (int row, int col, boolean putPieces){
        this.row = row;
        this.col = col;
        this.chessBoard = new ChessPiece[row][col];
        if(putPieces){
            this.putPieces();
        }

    }

    /**
     * Automates putting the pieces as they normally would be on the chess board
     */
    public void putPieces(){
        for(int x = 0; x <= row/2; x++){
            for(int y = 0; y < col; y++){
                //put non-pawn pieces
                if(x == 0){
                    //rook
                    if(y == 0 || y == col - 1){
                        this.chessBoard[x][y] = new Rook(x,y, PieceColor.WHITE);
                        this.chessBoard[row - 1- x][y] = new Rook(row - 1 - x, y , PieceColor.BLACK);
                        //this.chessBoard[x][y] = new ChessSpot(x,y, new Rook(0,0, true));
                        //this.chessBoard[row - 1- x][y] = new ChessSpot(row - 1- x,y,new Rook(row - 1 -x,y, false));
                    }
                    //knight
                    else if(y == 1 || y == col - 2){
                        this.chessBoard[x][y] = new Knight(x,y, PieceColor.WHITE);
                        this.chessBoard[row - 1 - x][y] = new Knight(row - 1- x, y, PieceColor.BLACK);
                    }
                    //bishop
                    else if(y == 2 || y == col - 3){
                        this.chessBoard[x][y] = new Bishop(x,y, PieceColor.WHITE);
                        this.chessBoard[row - 1 - x][y] = new Bishop(row - 1 - x, y, PieceColor.BLACK);
                    }
                    //queen
                    else if(y == 3){
                        this.chessBoard[x][y] = new Queen(x,y, PieceColor.WHITE);
                        this.chessBoard[row - 1 - x][y] = new Queen(row - 1 - x,y, PieceColor.BLACK);
                    }
                    //king
                    else if(y == 4){
                        this.chessBoard[x][y] = new King(x,y, PieceColor.WHITE);
                        this.chessBoard[row - 1 - x][y] = new King(x,y, PieceColor.BLACK);
                        this.blackKingX = row - 1 - x;
                        this.blackKingY = y;
                        this.whiteKingX = x;
                        this.whiteKingY = y;
                    }

                }
                //put pawns
                else if(x == 1){
                    this.chessBoard[x][y] = new Pawn(x,y, PieceColor.WHITE);
                    this.chessBoard[row - 1 - x][y] = new Pawn(row - 1 - x,y, PieceColor.BLACK);
                }else{
                    this.chessBoard[x][y] = null;
                    this.chessBoard[row - 1 - x][y] = null;
                }

            }
        }
    }

    /**
     *
     * @param color
     * @return returns King colors X location
     */
    public int getXKingLocation(PieceColor color){
        return (color == PieceColor.WHITE ? this.whiteKingX : this.blackKingX);
    }

    /**
     *
     * @param color
     * @return returns King colors Y location
     */
    public int getYKingLocation(PieceColor color){
        return (color == PieceColor.WHITE ? this.whiteKingY : this.blackKingY);
    }

    /**
     *
     * @param moveFromX Piece that we want to move at X coordinate
     * @param moveFromY Piece that we want to move at Y coordinate
     * @param moveToX  Where we want to move piece to X coordinate
     * @param moveToY Where we want to move piece to Y coordiante
     * @return allowed to move piece to given coordinate
     */
    public boolean movePiece(int moveFromX, int moveFromY, int moveToX, int moveToY){
        ChessPiece chessPiece = this.chessBoard[moveFromX][moveFromY];
        //System.out.println(chessPiece.getCoordinates()[0]);
        //System.out.println(chessPiece.getCoordinates()[1]);
        if(chessPiece == null){
            System.out.println("not a valid move");
            return false;
        }


        int rows = this.getRow();
        int cols = this.getCol();

        return chessPiece.makeMove(this, moveToX, moveToY);
    }

    //1 - white king is in check
    //2 - black king is in check
    //0 - no one is in check
    //-1 - invalid move
    //3 - checkmate

    /**
     *
     * @param moveFromX Piece that we want to move at X coordinate
     * @param moveFromY Piece that we want to move at Y coordinate
     * @param moveToX  Where we want to move piece to X coordinate
     * @param moveToY Where we want to move piece to Y coordiante
     * @return whether and what king is in check
     */
    public int inCheck(int moveFromX, int moveFromY, int moveToX, int moveToY){
        ChessPiece movingChessPiece = this.chessBoard[moveFromX][moveFromY];
        if(movingChessPiece == null){
            return -1;
        }
        System.out.println(movingChessPiece.getCoordinates()[0]);
        System.out.println(movingChessPiece.getCoordinates()[1]);
        assert(movingChessPiece != null);

        int rows = this.getRow();
        int cols = this.getCol();

        int whiteKingX = this.getXKingLocation(PieceColor.WHITE);
        int whiteKingY = this.getYKingLocation(PieceColor.WHITE);
        int blackKingX = this.getXKingLocation(PieceColor.BLACK);
        int blackKingY = this.getYKingLocation(PieceColor.BLACK);

        //moving white piece
        ChessPiece pieceAtSpotMovingTo = this.chessBoard[moveToX][moveToY];


        if(!movingChessPiece.makeMove(this,moveToX,moveToY)){
            System.out.println("Invalid move");
            return -1;
        }
        for(int x = 0; x < rows; x++){
            for(int y = 0; y < cols; y++){
                ChessPiece checkingPiece = this.chessBoard[x][y];
                if(checkingPiece != null && !( x == whiteKingX && y == whiteKingY) && !( x == blackKingX && y == blackKingY)){
                    if(checkingPiece.getColor() != pieceAtSpotMovingTo.getColor() && checkingPiece.isValidMove(this, movingChessPiece.getColor() == PieceColor.WHITE ? whiteKingX : blackKingX, movingChessPiece.getColor() == PieceColor.WHITE ? whiteKingY : blackKingY )){
                        this.chessBoard[moveFromX][moveFromY] = movingChessPiece;
                        this.chessBoard[moveToX][moveToY] = pieceAtSpotMovingTo;
                        return -1;
                    }else if(checkingPiece.getColor() == movingChessPiece.getColor() && checkingPiece.isValidMove(this, movingChessPiece.getColor() == PieceColor.WHITE ? whiteKingX : blackKingX, movingChessPiece.getColor() == PieceColor.BLACK ? whiteKingY : blackKingY)){
                        return checkingPiece.getColor() == PieceColor.WHITE ? 2 : 1;
                    }
                }
            }
        }

        return 0;
    }


    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @return chesspiece at x,y coordinate
     */
    public ChessPiece getChessPiece(int x, int y){
        if(x >= this.row || y >= this.col || x < 0 || y < 0){
            System.out.println("Invalid space");
            return null;
        }else{
            return chessBoard[x][y];
        }
    }

    /**
     *
     * @return number of columns
     */
    public int getCol() {
        return this.col;
    }

    /**
     *
     * @return number of rows
     */
    public int getRow() {
        return this.row;
    }

    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @return whether space is free at given x,y coordinate
     */
    public boolean isFree(int x, int y){
        return getChessPiece(x,y) == null;
    }

    /**
     *
     * @return chessboard
     */
    public ChessPiece[][] getChessBoard() {
        return chessBoard;
    }
}
