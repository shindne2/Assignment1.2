import java.util.Arrays;
import java.util.Scanner;


public class StartGame{
    public static void main(String args[]) {
        ChessBoard chessBoard = new ChessBoard(8, 8, true);
        printGame(chessBoard);

        //true is whites turn
        //false is blacks turn
        boolean startPlayer = true;
        //printGame(chessBoard);
        /*while(true){
            Scanner sc = new Scanner(System.in);
            System.out.println((startPlayer? "White": "Black") + " players turn");
            System.out.println("X coordinate of piece you want to move");
            int fromX =sc.nextInt();
            System.out.println("Y coordinate of piece you want to move");
            int fromY = sc.nextInt();
            if (chessBoard.getChessPiece(fromX,fromY)!= null && chessBoard.getChessPiece(fromX, fromY).isWhite() == startPlayer){
                System.out.println("X coordinate of where you want to move piece to");
                int toX = sc.nextInt();
                System.out.println("Y coordinate of where you want to move piece to");
                int toY = sc.nextInt();
                if(chessBoard.movePiece(fromX, fromY, toX, toY)){
                    startPlayer = !startPlayer;
                }
                printGame(chessBoard);
            }else{
                System.out.println("Either out of bounds or not your piece");
            }



        }*/
    }

    /**
     *
     * @param cb current chessboard
     * prints how the chessboard looks
     */
    public static void printGame(ChessBoard cb){
        for(int i = 0; i < 8; i++){
            for(int k = 0; k < 8; k++){
                ChessPiece cp = cb.getChessBoard()[i][k];
                if(cp == null){
                    System.out.print('.');
                }else {
                    System.out.print(cp.boardCharacter());
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}