

import junit.framework.TestSuite;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;




public class TestPieceMoves extends TestSuite {


    @Test
    public void testPawnMovementForward(){
        ChessBoard chessBoard = new ChessBoard(8,8, false);
        ChessPiece pawn = new Pawn(1,1,PieceColor.WHITE);
        chessBoard.getChessBoard()[1][1] = pawn;

        pawn.makeMove(chessBoard, 3,1);
        assertEquals(pawn, chessBoard.getChessBoard()[3][1]);
        pawn.makeMove(chessBoard, 4,1);
        assertEquals(pawn,chessBoard.getChessBoard()[4][1]);
        assertEquals(null, chessBoard.getChessBoard()[3][1]);
        pawn.makeMove(chessBoard, 6, 1);
        assertEquals(pawn, chessBoard.getChessBoard()[4][1]);
    }

    @Test
    public void testPawnAttack(){
        ChessBoard chessBoard = new ChessBoard(8,8,false);
        ChessPiece wPawn = new Pawn(3,3,PieceColor.WHITE);
        ChessPiece bPawn = new Pawn(4,4,PieceColor.BLACK);

        chessBoard.getChessBoard()[3][3] = wPawn;
        chessBoard.getChessBoard()[4][4] = bPawn;

        wPawn.makeMove(chessBoard, 4,4);
        assertEquals(wPawn, chessBoard.getChessBoard()[4][4]);

    }



    @Test

    public void testKnightMovement(){
        ChessBoard chessBoard = new ChessBoard(8,8,false);
        ChessPiece wKnight = new Knight(0,1,PieceColor.WHITE);
        chessBoard.getChessBoard()[0][1] = wKnight;

        assertEquals(false, wKnight.makeMove(chessBoard,2,1));
        assertEquals(true, wKnight.makeMove(chessBoard, 2,2));
        assertEquals(chessBoard.getChessBoard()[2][2], wKnight);
        assertEquals(false, wKnight.makeMove(chessBoard,-1,-1));
        assertEquals(chessBoard.getChessBoard()[2][2], wKnight);
    }

    //public void testKnightAttack()
    @Test
    public void testKnightAttack(){
        ChessBoard chessBoard = new ChessBoard(8,8,false);
        ChessPiece wKnight = new Knight(0,1,PieceColor.WHITE);
        chessBoard.getChessBoard()[0][1] = wKnight;

        ChessPiece wPawn = new Pawn(2,0,PieceColor.WHITE);
        chessBoard.getChessBoard()[2][0] = wPawn;
        ChessPiece bPawn = new Pawn(2,2,PieceColor.BLACK);
        chessBoard.getChessBoard()[2][2] = bPawn;

        assertEquals(false, wKnight.makeMove(chessBoard, 2,0));
        assertEquals(true, wKnight.makeMove(chessBoard, 2,2));
        assertEquals(wKnight, chessBoard.getChessBoard()[2][2]);


    }

    @Test
    public void testBishopMovementAndAttack(){
        ChessBoard cb= new ChessBoard(8,8,false);
        ChessPiece wBishop = new Bishop(0,2, PieceColor.WHITE);

        cb.getChessBoard()[0][2] = wBishop;
        cb.getChessBoard()[1][3] = new Pawn(1,3,PieceColor.BLACK);
        assertEquals(false, wBishop.makeMove(cb, 4,6));
        assertEquals(true, wBishop.makeMove(cb,1,3));
        assertEquals(true, wBishop.makeMove(cb,4,6));
        assertEquals(false, wBishop.makeMove(cb, -1, -1));
        assertEquals(wBishop, cb.getChessBoard()[4][6]);


    }

    @Test
    public void testRookMovementAndAttack(){
        ChessBoard cb= new ChessBoard(8,8,false);
        ChessPiece wRook = new Rook(0,0, PieceColor.WHITE);

        cb.getChessBoard()[0][0] = wRook;
        cb.getChessBoard()[1][3] = new Pawn(1,3,PieceColor.BLACK);
        cb.getChessBoard()[2][4] = new Pawn(2,4,PieceColor.WHITE);
        assertEquals(false, wRook.makeMove(cb, 4,6));
        assertEquals(false, wRook.makeMove(cb, -1,-1));
        assertEquals(true, wRook.makeMove(cb, 5,0));
        assertEquals(true, wRook.makeMove(cb, 5,5));
        assertEquals(true, wRook.makeMove(cb, 5,4));
        assertEquals(false, wRook.makeMove(cb, 2,4));
        assertEquals(true, wRook.makeMove(cb, 5,3));
        assertEquals(true, wRook.makeMove(cb, 1,3));

    }

    @Test
    public void testQueenMovementAndAttack(){
        ChessBoard cb= new ChessBoard(8,8,false);
        ChessPiece wQueen = new Queen(0,2, PieceColor.WHITE);

        cb.getChessBoard()[0][2] = wQueen;
        cb.getChessBoard()[1][3] = new Pawn(1,3,PieceColor.BLACK);
        cb.getChessBoard()[2][4] = new Pawn(2,4,PieceColor.WHITE);
        assertEquals(true, wQueen.makeMove(cb,0,0));
        assertEquals(false, wQueen.makeMove(cb, -1,-1));
        assertEquals(wQueen, cb.getChessBoard()[0][0]);
        assertEquals(true, wQueen.makeMove(cb, 1,1));
        assertEquals(null, cb.getChessBoard()[0][0]);
        assertEquals(wQueen, cb.getChessBoard()[1][1]);
        assertEquals(true,wQueen.makeMove(cb,1,3));
        assertEquals(null, cb.getChessBoard()[1][1]);
        assertEquals(wQueen, cb.getChessBoard()[1][3]);
        assertEquals(false, wQueen.makeMove(cb, 2, 4));
        assertEquals(wQueen, cb.getChessBoard()[1][3]);
        assertEquals(true, wQueen.makeMove(cb,5,3));
        assertEquals(wQueen, cb.getChessBoard()[5][3]);
    }






}
