import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestNewPiecesMovesAndAttacks {


    @Test
    public void testBigKnightMove(){
        ChessBoard cb = new ChessBoard(8,8, false);
        ChessPiece wBigKnight = new BigKnight(3,3, PieceColor.WHITE);

        cb.getChessBoard()[3][3] = wBigKnight;
        cb.getChessBoard()[1][3] = new Pawn(1,3, PieceColor.WHITE);
        assertEquals(true, wBigKnight.makeMove(cb, 0,1));
        assertEquals(false, wBigKnight.makeMove(cb, -1,-1));
        assertEquals(true, wBigKnight.makeMove(cb,2,4));

    }

    @Test
    public void testBigKnightAttack(){
        ChessBoard cb = new ChessBoard(8,8, false);
        ChessPiece wBigKnight = new BigKnight(3,3, PieceColor.WHITE);

        cb.getChessBoard()[5][6] = new Pawn(5,6,PieceColor.WHITE);
        cb.getChessBoard()[1][6] = new Pawn(1,6,PieceColor.BLACK);
        cb.getChessBoard()[3][4] = new Pawn(3,4,PieceColor.BLACK);

        assertEquals(false, wBigKnight.makeMove(cb, 5,6));
        assertEquals(true, wBigKnight.makeMove(cb, 1,6));
        assertEquals(false, wBigKnight.makeMove(cb, 3,4));
    }


    @Test
    public void testHalfBishopMovement(){
        ChessBoard cb = new ChessBoard(8,8, false);
        ChessPiece wHalfBishop  = new HalfBishop(4,4, PieceColor.WHITE);

        cb.getChessBoard()[4][4] = wHalfBishop;

        cb.getChessBoard()[2][2] = new Pawn(2,2, PieceColor.BLACK);
        cb.getChessBoard()[6][6] = new Pawn(6,6, PieceColor.WHITE);

        assertEquals(false, wHalfBishop.makeMove(cb, 6,6));
        assertEquals(true, wHalfBishop.makeMove(cb,2,2) );
        assertEquals(false, wHalfBishop.makeMove(cb,1,3));
        assertEquals(wHalfBishop, cb.getChessBoard()[2][2]);
        StartGame.printGame(cb);


    }
}
