public class Queen extends ChessPiece {
    private int x;
    private int y;
    private PieceColor color;

    /**
     *
     * @param x starting x coordinate
     * @param y starting y coordinate
     * @param color of piece
     */
    public Queen(int x, int y, PieceColor color){
        super(x,y,color);
    }

    /**
     *
     * @param chessBoard we are playing to
     * @param moveToX where we want to move piece to x
     * @param moveToY where we want to move piece to y
     * @return whether move is valid
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        int rows = chessBoard.getRow();
        int cols = chessBoard.getCol();

        int checkX = super.x;
        int checkY = super.y;

        if((checkX == moveToX && checkY == moveToY) || moveToX >= rows || moveToX < 0 || moveToY > cols || moveToY < 0 ){
            return false;
        }
        ChessPiece cp = chessBoard.getChessBoard()[moveToX][moveToY];
        if(cp!= null && cp.getColor() == this.getColor()){
            return false;
        }

        return checkQueenMoves(chessBoard, checkX, checkY, moveToX, moveToY);

    }

    /**
     *
     * @param chessBoard we are playing on
     * @param startX where the piece is starting at x
     * @param startY where the piece is starting at y
     * @param moveToX where the piece is going to x
     * @param moveToY where the piece is going to y
     * @return whether move is legal i.e. checks rook and bishop moves
     */
    public boolean checkQueenMoves(ChessBoard chessBoard, int startX, int startY, int moveToX, int moveToY){
        return checkColsRows(chessBoard, startX, startY, moveToX, moveToY) || checkDiagonal(chessBoard, startX, startY,moveToX, moveToY);
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param startX where the piece is starting at x
     * @param startY where the piece is starting at y
     * @param endX where the piece is going to x
     * @param endY where the piece is going to y
     * @return whether move is legal i.e. checks bishop rules
     */
    public boolean checkDiagonal(ChessBoard chessBoard, int startX, int startY, int endX, int endY){
        int dx = (endX - startX)/Math.abs(endX - startX);
        int dy = (endY- startY)/Math.abs(endY - startY);
        int x = startX;
        int y = startY;
        //check diagnoal and see if its free upto point we want to move to
        if(Math.abs(startX - endX) != Math.abs(startY - endY)){
            return false;
        }
        while(x != endX - dx){
            x += dx;
            y += dy;
            if(chessBoard.getChessBoard()[x][y] != null){
                return false;
            }
        }
        //check if space we want to go to is either free or opposing players piece is on it
        ChessPiece cp = chessBoard.getChessBoard()[endX][endY];
        if(cp == null){
            return true;
        } else if(cp != null && cp.getColor() == this.getColor()){
            return false;
        }else {
            return true;
        }
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param startX where the piece is starting at x
     * @param startY where the piece is starting at y
     * @param endX where the piece is going to x
     * @param endY where the piece is going to y
     * @return whether move is legal i.e. checks rook moves
     * @return
     */
    public boolean checkColsRows(ChessBoard chessBoard, int startX, int startY, int endX, int endY){
        if(startX == endX && startY == endY){
            return false;
        }
        if(startX != endX && startY != endY){
            return false;
        }

        int dx = (endX == startX) ? 0 :  (endX - startX)/ Math.abs(endX - startX);
        int dy = (endY == startY) ? 0 : (endY - startY) / Math.abs(endY - startY);

        int x = startX;
        int y = startY;

        while(!(endX -dx == x || endY -dy == y)){
            x += dx;
            y += dy;
            if(chessBoard.getChessBoard()[x][y] != null) {
                return false;
            }
        }
        if(chessBoard.getChessBoard()[endX][endY] != null && chessBoard.getChessBoard()[endX][endY].getColor() == this.getColor() ){
            return false;
        }
        return true;
    }

    /**
     * character representation on board
     * @return
     */
    @Override
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'q';
        }
        return 'Q';
    }
}
