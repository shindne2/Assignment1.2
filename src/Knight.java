public class Knight extends ChessPiece {
    private int x;
    private int y;
    private static PieceColor color;

    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @param color of piece
     */
    public Knight(int x, int y, PieceColor color){

        super(x,y,color);
        this.x = x;
        this.y = y;
        this.color = color;
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX where we want piece to move x
     * @param moveToY where we want piece to move y
     * @return
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        int rows = chessBoard.getRow();
        int cols = chessBoard.getCol();

        if(moveToX >= rows || moveToX < 0 || moveToY >= cols || moveToY < 0){
            return false;
        }

        ChessPiece cp = chessBoard.getChessBoard()[moveToX][moveToY];
        if((Math.abs(this.x - moveToX) == 2 && Math.abs(this.y - moveToY) == 1) || (Math.abs(this.x - moveToX) == 1 && Math.abs(this.y - moveToY) == 2)){
            if(cp == null || cp.getColor() != this.getColor()){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }
    /**
     * character representation on board
     * @return
     */
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'n';
        }
        return 'N';
    }
}
