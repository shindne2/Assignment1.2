public class King extends ChessPiece{
    private int x;
    private int y;
    private static PieceColor color;

    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @param color of piece
     */
    public King(int x, int y, PieceColor color){
        super(x,y,color);
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX where we want to move to x
     * @param moveToY where we want to move to y
     * @return checks if valid move for king
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        int rows = chessBoard.getRow();
        int cols = chessBoard.getCol();

        if(moveToX >= rows || moveToX < 0 || moveToY >= cols || moveToY < 0 || (moveToX == this.x && moveToY == this.y) ){
            return false;
        }else if(Math.abs(moveToX - this.x) > 1 || Math.abs(moveToY - this.y) > 1 || (moveToX == this.x && moveToY == this.y)){
            return false;
        }else if(chessBoard.getChessBoard()[moveToX][moveToY] == null || chessBoard.getChessBoard()[moveToX][moveToY].getColor() != this.getColor()){
            return true;
        }else{
            return false;
        }

    //implement in check on top of all characterisitics
    }

    /**
     * character representation on board
     * @return
     */
    @Override
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'k';
        }
        return 'K';
    }

}
