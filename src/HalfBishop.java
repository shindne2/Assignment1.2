public class HalfBishop extends ChessPiece{
    private int x;
    private int y;
    private PieceColor color;


    public HalfBishop(int x, int y, PieceColor color){
        super(x,y,color);
        this.x = x;
        this.y = y;
        this.color = color;

    }


    /**
     *
     * @param chessBoard itself
     * @param moveToX where to move to x coordinate
     * @param moveToY where to move to y coordinate
     * @return whether given move is a valid move
     */
    @Override
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        int rows = chessBoard.getRow();
        int cols = chessBoard.getCol();

        if(Math.abs(this.x - moveToX) != Math.abs(this.y - moveToY) || moveToX >= rows || moveToX < 0 || moveToY >= cols || moveToY < 0) {
            return false;
        }
        int checkX = super.x;
        int checkY = super.y;

        return checkDiagonal(chessBoard, checkX, checkY, moveToX, moveToY);


    }

    /**
     *
     * @param chessBoard itself
     * @param startX where chesspiece is starting x coordinate
     * @param startY where chesspiece is starting y coordinate
     * @param endX where chesspiece is ending x coordinate
     * @param endY where chesspiece is ending
     * @return checks one diagonal and see if its a valid move
     */

    public boolean checkDiagonal(ChessBoard chessBoard, int startX, int startY, int endX, int endY){
        int dx = (endX - startX)/Math.abs(endX - startX);
        int dy = (endY- startY)/Math.abs(endY - startY);
        if(dx != dy){
            return false;
        }
        int x = startX;
        int y = startY;
        //check diagnoal and see if its free upto point we want to move to
        while(x != endX - dx){
            x += dx;
            y += dy;
            if(!chessBoard.isFree(x,y)) {
                return false;
            }
        }
        //check if space we want to go to is either free or opposing players piece is on it
        if(chessBoard.isFree(endX, endY)){
            return true;
        }
        else if(!chessBoard.isFree(endX, endY) && chessBoard.getChessPiece(endX,endY).getColor() == this.color){
            return false;
        }else{
            return true;
        }



    }

    /**
     *
     * @return character representation on board
     */
    @Override
    public char boardCharacter(){
        if(this.getColor() == PieceColor.WHITE){
            return 'b';
        }
        return 'B';
    }
}
