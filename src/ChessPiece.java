/**
 * Piece Color
 */
enum PieceColor{
    WHITE, BLACK
}

public class ChessPiece {
    public int x;
    public int y;
    private PieceColor color;

    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @param color of piece
     */
    public ChessPiece(int x, int y, PieceColor color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    /**
     *
     * @param chessBoard we are playing on
     * @param moveToX
     * @param moveToY
     * @return
     */
    public boolean makeMove(ChessBoard chessBoard, int moveToX, int moveToY){
        if(this.isValidMove(chessBoard, moveToX, moveToY)){
            chessBoard.getChessBoard()[this.x][this.y] = null;
            chessBoard.getChessBoard()[moveToX][moveToY] = this;
            this.x = moveToX;
            this.y = moveToY;
            return true;
        }else{
            System.out.println("Invalid move to (" + moveToX + "," + moveToY + ") with " + this.getClass().toString());
            return false;
        }
    }

    /**
     * board representation of typical piece or null
     * needs to be overridden
     * @return
     */
    public char boardCharacter(){
        return '.';
    }


    /**
     *
     * @param chessBoard we are playing to
     * @param moveToX where we want to move piece to x
     * @param moveToY where we want to move piece to y
     * @return
     */
    public boolean isValidMove(ChessBoard chessBoard, int moveToX, int moveToY){
        return false;
    }

    /**
     * gets coordinaes of piece in array of length 2
     * @return
     */
    public int[] getCoordinates(){
        int [] coord = {x, y};
        return coord;
    }

    /**
     * gets pieces color
     * @return
     */
    public PieceColor getColor(){
        return this.color;
    }
    // put more functions here
}